import micropython
import gc
micropython.mem_info(1)
gc.collect()
gc.threshold(gc.mem_free() // 4 + gc.mem_alloc())
import json
import utime
import machine
from machine import Pin, UART
from umodbus.serial import Serial as ModbusRTUMaster
import uasyncio as asyncio
import LoRaWANHandler
from LoRaConfig import LoRaConfig

# define the pins for the Modbus RTU communication
rtu_pins = (Pin(4), Pin(5))

# load the configuration from the config file
with open('config.json', 'r') as f:
    config = json.load(f)

# read the configuration from config.json
slaves = config['MODBUS']['Slaves']
starting_address = config['MODBUS']['StartingAddress']
qty = config['MODBUS']['Quantity']

host = ModbusRTUMaster(baudrate=9600, data_bits=8, stop_bits=1, parity=None, pins=rtu_pins, ctrl_pin=None, uart_id=1)

LED_PIN = const(25)
led = machine.Pin(LED_PIN, machine.Pin.OUT)

lh = LoRaWANHandler.LoRaWANHandler(LoRaConfig)


def blink(count, delay):
    for _ in range(count):
        led.on()
        utime.sleep_ms(delay // 2)
        led.off()
        utime.sleep_ms(delay // 2)


def create_payload(value01, value02):
    lw_payload = {'tr1': value01, 'tr2': value02}
    return json.dumps(lw_payload)


async def read_input_registers_async(slave_addr, starting_addr, register_qty):
    try:
        await asyncio.sleep(0)  # yield control to the event loop
        power_total = 0
        for i in starting_addr:
            values = host.read_input_registers(slave_addr=slave_addr, starting_addr=i,
                                               register_qty=register_qty, signed=False)
            if values[0] is not None:
                if values[0] > 65000:
                    power_total += values[0] - 65535
                else:
                    power_total += values[0]
            else:
                power_total += 0
        return power_total
    except Exception as e:
        print(f'An error occurred while reading from slave {slave_addr}:', e)
        return None


print("This is the LoRa Modbus Gateway")
LoRaWANHandler.getBoardID()

blink(3, 1000)
# lh.sendABP()
blink(5, 1000)
utime.sleep_ms(5000)


async def main():
    while True:
        try:
            trafo1_task = asyncio.create_task(read_input_registers_async(slaves[0], starting_address, qty))
            trafo2_task = asyncio.create_task(read_input_registers_async(slaves[1], starting_address, qty))
            trafo1 = await trafo1_task
            trafo2 = await trafo2_task
            payload = create_payload(trafo1, trafo2)
            print('Payload:', payload)
#            lh.sendUnconfirmed(payload)
            lh.sendABP(payload)
            blink(1, 1000)
            await asyncio.sleep(30)
        except OSError as e:
            print('A communication error occurred:', e)
            blink(10, 100)
            await asyncio.sleep(3)
            continue
        except ValueError as e:
            print('A value error occurred:', e)
            blink(10, 100)
            await asyncio.sleep(3)
            continue
        except Exception as e:
            print('An unexpected error occurred:', e)
            blink(10, 100)
            await asyncio.sleep(3)
            continue

# Run the main function until it completes
asyncio.run(main())


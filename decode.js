function decodeUplink(input) {
    const decoded = Buffer.from(input.bytes, 'base64').toString('hex');
    const data = Buffer.from(decoded, 'hex').toString();
    // parse the string as JSON
    const data1 = JSON.parse(data);
    // if data.tr1 and data.tr2 are not null, convert them to megawatt hours
    if (data1.tr1 !== null) {
        data1.tr1 = data1.tr1 / 1000;
    }
    if (data1.tr2 !== null) {
        data1.tr2 = data1.tr2 / 1000;
    }
    data1.total = data1.tr1 + data1.tr2;
    // return the object
    return {
        data: {
            tr1: data1.tr1,
            tr2: data1.tr2,
            total: data1.total
        }
    };
}
